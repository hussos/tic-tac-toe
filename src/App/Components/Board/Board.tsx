import React from 'react';
import Cell from "../Cell/Cell";
import useBoardStyles from "./BoardStyles";

const Board = () => {

    const classes = useBoardStyles();

    return (
        <div className={classes.container}>
            <div>
                <Cell index={0}/>
                <Cell index={1}/>
                <Cell index={2}/>
            </div>
            <div>
                <Cell index={3}/>
                <Cell index={4}/>
                <Cell index={5}/>
            </div>
            <div>
                <Cell index={6}/>
                <Cell index={7}/>
                <Cell index={8}/>
            </div>
        </div>
    );
};

export default Board;
