import { makeStyles, Theme } from "@material-ui/core";

const useBoardStyles = makeStyles<Theme>( ( theme: Theme ) => ({
    container: {
        display: "flex",
        flexDirection: "column",
        width: "300px"
    },
}));




export default useBoardStyles;
