import React, {useState} from 'react';
import {connect} from "react-redux";
import X from '@material-ui/icons/Close';
import O from '@material-ui/icons/RadioButtonUnchecked';
import {IconButton} from "@material-ui/core";
import useCellStyles from "./CellStyles";
import {makeMove} from "../../Redux/actions/actions";
import {getGameStatus, getMove} from "../../Redux/selector/selector";

interface CellProps {
    index: number;
    end: boolean;
    move: string;
    makeMove: (move: {move: string, index: number}) => void;
}

const Cell: React.FC<CellProps> = props => {

    const {makeMove, move, index, end} = props;

    const [currMove, setCurrMove] = useState<string>('');

    const classes = useCellStyles();

    const handleClick = () => {
        setCurrMove(move === 'x' ? 'o' : 'x');
        makeMove({move: move, index: index});
    };

    return (
        <IconButton
            onClick={handleClick}
            disabled={!!currMove || end}
            className={classes.cell}
        >
            {
                currMove === 'x' && <X/>
            }
            {
                currMove === 'o' &&  <O/>
            }
        </IconButton>
    );
};

const mapStateToProps = state => ({
    move: getMove(state),
    end: getGameStatus(state)
});

const mapDispatchToProps = dispatch => ({
    makeMove: (move) =>  dispatch(makeMove(move)),
});

export default connect(mapStateToProps, mapDispatchToProps) (Cell);
