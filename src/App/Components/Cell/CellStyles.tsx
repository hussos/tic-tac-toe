import { makeStyles, Theme } from "@material-ui/core";

const useCellStyles = makeStyles<Theme>( ( theme: Theme ) => ({
    cell: {
        border: "1px solid #000",
        width: "100px",
        height: "100px"
    }
}));

export default useCellStyles;
