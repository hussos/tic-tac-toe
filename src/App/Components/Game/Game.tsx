import React from 'react';
import {connect} from "react-redux";
import Board from "../Board/Board";
import {getGameStatus, getMove} from "../../Redux/selector/selector";
import useGameStyles from "./GameStyles";

interface GameProps {
    move: string;
    end: boolean;
}

const Game: React.FC<GameProps> = props => {

    const {move, end} = props;

    const classes = useGameStyles();

    let currPlay = move === 'o' ? 'X' : 'O';

    return (
        <div className={classes.gameContainer}>
            <Board/>
            {
                !end
                && <h2 className={classes.prompt}>It's {currPlay}'s turn</h2>
            }
            {
                end
                && <h2 className={classes.prompt}>{move.toUpperCase()} wins!</h2>
            }
        </div>
    );
};
const mapStateToProps = state => ({
    move: getMove(state),
    end: getGameStatus(state)
});

export default connect(mapStateToProps) (Game);
