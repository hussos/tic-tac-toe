import { makeStyles, Theme } from "@material-ui/core";

const useGameStyles = makeStyles<Theme>( ( theme: Theme ) => ({
    gameContainer: {
        width: "450px",
        margin: "200px auto",
        display: "flex"
    },
    prompt: {
        paddingLeft: "20px"
    }
}));




export default useGameStyles;
