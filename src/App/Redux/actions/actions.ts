import {MAKE_MOVE} from "./actionTypes";

export const makeMove = content => ({
        type: MAKE_MOVE,
        payload: {
            content
        }
});
