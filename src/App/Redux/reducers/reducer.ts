import {MAKE_MOVE} from "../actions/actionTypes";

const initialState = {
    move: 'o',
    board: [
        '1', '2', '3',
        '4', '5', '6',
        '7', '8', '9'
    ],
    end: false
};

/**
 *
 * @param state
 * @param action
 */
export default function (state = initialState, action) {
    switch (action.type) {
        case MAKE_MOVE: {
            const {content} = action.payload;

            let move = content.move === 'x' ? 'o' : 'x';
            let updatedBoard = state.board.slice();
            updatedBoard.splice(content.index, 1, move);

            let didGameEnd = checkGameStatus(updatedBoard);

            return {
                ...state,
                move: move,
                board: [...updatedBoard],
                end: didGameEnd
            };
        }
        default:
            return state;
    }
}

const checkGameStatus = (board: string[]) => {

    if (board[0] === board[1] && board[1] === board[2])
        return true;
    if (board[3] === board[4] && board[4] === board[5])
        return true;
    if (board[6] === board[7] && board[7] === board[8])
        return true;
    if (board[0] === board[3] && board[3] === board[6])
        return true;
    if (board[1] === board[4] && board[4] === board[7])
        return true;
    if (board[2] === board[5] && board[5] === board[8])
        return true;
    if (board[0] === board[4] && board[4] === board[8])
        return true;

    return !!(board[2] === board[4] && board[4] === board[6]);
};
